var AP = {} || AP;

AP.selectWrapper = function () {
    var sel = $("select"),
        selWrap = $("<div class='select-wrapper' />");

    sel.wrap(selWrap);
};

AP.dataTables = function (tblObject, options, callback) {
    var opts = {
		"order": [[ 0, "desc" ]],
		"aLengthMenu": [
			[3, 20, 60, 100, -1],
			[3, 20, 60, 100, "All"]
		],
        "oLanguage": {
            "oPaginate": {
                "sPrevious": "<i class='fa fa-chevron-left'  aria-hidden='true'></i>",
                "sNext": "<i class='fa fa-chevron-right'  aria-hidden='true'></i>"
            }
        },
        "fnDrawCallback": callback
	};
	$.extend(opts, options);
	$(tblObject).DataTable(opts);

	$(tblObject).parent().find(".dataTables_filter input[type='search']").attr("placeholder", "Search your transactions...");
    $(tblObject).parent().find("select, input[type='search']").addClass("form-control");
};

AP.accordion = function (obj) {
    var dl = $(obj),
        dt = dl.find("dt"),
        dd = dl.find("dd");

    dd.eq(0).show();
    dt.on("click", "label", function (e) {
        e.stopPropagation();
        $("dd").slideUp();
        $(this).parent().next("dd").slideDown();
    });
};

$(function () {
	AP.selectWrapper();
    // AP.dataTables(".data-table");
    AP.accordion(".accordion");
});
