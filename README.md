# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Blank Handlebars source code directory to start with new project.

### How do I get set up? ###

Just clone the repository and run npm install to get dependency.

- grunt serve: To Run the server locally
- grunt build: To build production directory

### Change Git Repo URL ###

After you have cloned the repo to your project directory you need to change the Git Repo URL to your Git repo.

- git remote set-url origin git://new.url.here

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Owner: Ashish Panchal
URL: http://ashishuideveloper.in